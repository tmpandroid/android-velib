package epita.velibmapper.shared;

import epita.velibmapper.dbo.Data;

/**
 * Created by Quentin on 18/05/2017.
 */

public class GlobalDatas {


    private static Data datas = new Data();
    private static boolean nrr = false;

    public static Data getInstance() {
        return datas;
    }

    public static void SetData(Data data)
    {
        datas = data;
    }

    public static boolean GetNr() { return nrr; }
    public static void SetNr(boolean nr) { nrr = nr;}
}