package epita.velibmapper.shared;

/**
 * Created by Quentin on 23/05/2017.
 */

public class StationNameCleaner
{
    public static String Clean(String str)
    {
        String res[] = str.split("-", 2);
        if (res.length > 1)
        {
            if (res[1].length() > 1)
                return res[1].substring(1);
            else
                return res[1];
        }
        else
            return str;
    }
}
