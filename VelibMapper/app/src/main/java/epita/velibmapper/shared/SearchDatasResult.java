package epita.velibmapper.shared;

import java.util.ArrayList;

import epita.velibmapper.dbo.Data;
import epita.velibmapper.dbo.Record;

/**
 * Created by Quentin on 23/05/2017.
 */

public class SearchDatasResult
{
    private static Record[] datas = new Record[0];

    public static Record[] getInstance() {
            return datas;
        }

    public static void SetData(Record[] data)
        {
            datas = data;
        }
}
