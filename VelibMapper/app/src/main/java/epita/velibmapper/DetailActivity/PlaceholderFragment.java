package epita.velibmapper.DetailActivity;

import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.Console;
import java.text.SimpleDateFormat;
import java.util.Locale;

import epita.velibmapper.R;
import epita.velibmapper.dbo.Record;
import epita.velibmapper.shared.StationNameCleaner;

/**
 * Created by Quentin on 18/05/2017.
 */

public class PlaceholderFragment extends Fragment
{
    private static final String ARG_SECTION_NUMBER = "section_number";

    private static Record[] mRecord;
    public static void setDatas(Record[] rec)
    {
        mRecord = rec;
    }

    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_detail_fragment, container, false);
        TextView name = (TextView) rootView.findViewById(R.id.station_name);
        int pos = getArguments().getInt(ARG_SECTION_NUMBER) - 1;
        String station_name = StationNameCleaner.Clean(mRecord[pos].GetField().getName());
        name.setText(station_name);

        TextView status = (TextView) rootView.findViewById(R.id.status_text);
        String status_text = mRecord[pos].GetField().getStatus();
        set_status(status, status_text);

        TextView available = (TextView) rootView.findViewById(R.id.available_places);
        int available_stand = mRecord[pos].GetField().getAvailable_bike_stands();
        int total_stand = mRecord[pos].GetField().getBike_stands();
        if (available_stand == 0)
        {
            available.setTextColor(Color.RED);
        }
        String tmp = "Places disponibles : " + available_stand + "/" + total_stand;
        available.setText(tmp);

        TextView address = (TextView) rootView.findViewById(R.id.localization);
        address.setText(mRecord[pos].GetField().getAddress());
        initializeGoogleMap(address, pos);

        TextView lastUpdate = (TextView) rootView.findViewById(R.id.lastUpdate);
        String lastUpdateText = "Dernière mise à jour le : "
                + getDisplayableDate(mRecord[pos].GetField().getLast_update());
        lastUpdate.setText(lastUpdateText);

        return rootView;
    }

    private void set_status(TextView status_open, String status)
    {
        switch (status)
        {
            case "CLOSED":
                int text_color_red = ContextCompat.getColor(getContext(), R.color.colorTextRed);
                status_open.setTextColor(text_color_red);
                break;
            case "OPEN":
                int text_color_green = ContextCompat.getColor(getContext(), R.color.colorTextGreen);
                status_open.setTextColor(text_color_green);
                break;
            default:

                break;
        }
        status_open.setText(status);
    }

    private String getDisplayableDate(String date)
    {
        try
        {
            String[] separated = date.split("T|\\+");
            String res = separated[0] + " à ";
            res += separated[1];
            return res;
        }
        catch (Exception e)
        {
            return date;
        }
    }

    private void initializeGoogleMap(final TextView address, final int pos)
    {
        address.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String check = address.getText().toString();
                String map = "http://maps.google.co.in/maps?q=" + check;

                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                startActivity(i);
            }
        });
    }

    public static String ShareText(int item_position)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(mRecord[item_position].getFields().getName());
        sb.append('\n');
        int bikes_stands = mRecord[item_position].getFields().getBike_stands();
        int available_bikes_stands  = mRecord[item_position].getFields().getAvailable_bike_stands();
        sb.append("Nombre de places disponible : " + available_bikes_stands + "/" + bikes_stands);
        sb.append('\n');
        sb.append("addresse : " + mRecord[item_position].getFields().getAddress());
        return sb.toString();
    }
}