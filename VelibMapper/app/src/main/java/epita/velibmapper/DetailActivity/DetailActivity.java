package epita.velibmapper.DetailActivity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.Console;
import java.util.List;

import epita.velibmapper.contact_activity.ContactActivity;
import epita.velibmapper.dbo.*;
import epita.velibmapper.shared.GlobalDatas;
import epita.velibmapper.R;
import epita.velibmapper.shared.SearchDatasResult;
import epita.velibmapper.shared.Shared;

public class DetailActivity extends AppCompatActivity{

    ViewPager mViewPager;
    SectionsPagerAdapter mSectionsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mSectionsPagerAdapter.setDatas(SearchDatasResult.getInstance());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        Intent intent = getIntent();
        mViewPager.setCurrentItem(intent.getExtras().getInt(Shared.INDEX_ARG));

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_activity_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_contact:
                Intent intent = new Intent(getApplicationContext(), ContactActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_share:
                int pos = mViewPager.getCurrentItem();
                String sendContent = PlaceholderFragment.ShareText(pos);
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, sendContent);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                return pos >= 0;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}