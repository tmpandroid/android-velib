package epita.velibmapper.DetailActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import epita.velibmapper.dbo.*;

/**
 * Created by Quentin on 18/05/2017.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter
{
    int record_length;
    public void setDatas(Record[] rec)
    {
        record_length = rec.length;
        PlaceholderFragment.setDatas(rec);
    }

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return PlaceholderFragment.newInstance(position + 1);
    }

    @Override
    public int getCount() {
        return record_length;
    }

}
