package epita.velibmapper.dbo;

/**
 * Created by aboul on 18/05/2017.
 */

public class Geometry {
    private String type;
    private float[] coordinates;

    public Geometry (String type, float[] coordinates)
    {
        this.type = type;
        this.coordinates = coordinates;
    }

    //region GetterSetter

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(float[] coordinates) {
        this.coordinates = coordinates;
    }

    //endregion GetterSetter
}
