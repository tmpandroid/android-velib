package epita.velibmapper.dbo;

/**
 * Created by aboul on 18/05/2017.
 */

public class Fields {

    private String status;
    private String contract_name;
    private String name;
    private boolean bonus;
    private int bike_stands;
    private int number;
    private String last_update;
    private int available_bike_stands;
    private boolean banking;
    private int available_bikes;
    private String address;
    private float[] position;

    public Fields(String status, String contract_name, String name, boolean bonus, int bike_stands,
                  int number, String last_update, int available_bikes_stands, boolean banking,
                  int available_bikes, String address, float[] position)
    {
        this.status = status;
        this.contract_name = contract_name;
        this.name = name;
        this.bonus = bonus;
        this.bike_stands = bike_stands;
        this.number = number;
        this.last_update = last_update;
        this.available_bike_stands = available_bikes_stands;
        this.banking = banking;
        this.available_bikes = available_bikes;
        this.address = address;
        this.position = position;
    }

    //region GetterSetter

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContract_name() {
        return contract_name;
    }

    public void setContract_name(String contract_name) {
        this.contract_name = contract_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBonus() {
        return bonus;
    }

    public void setBonus(boolean bonus) {
        this.bonus = bonus;
    }

    public int getBike_stands() {
        return bike_stands;
    }

    public void setBike_stands(int bike_stands) {
        this.bike_stands = bike_stands;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public int getAvailable_bike_stands() {
        return available_bike_stands;
    }

    public void setAvailable_bike_stands(int available_bikes_stands) {
        this.available_bike_stands = available_bikes_stands;
    }

    public boolean isBanking() {
        return banking;
    }

    public void setBanking(boolean banking) {
        this.banking = banking;
    }

    public int getAvailable_bikes() {
        return available_bikes;
    }

    public void setAvailable_bikes(int available_bikes) {
        this.available_bikes = available_bikes;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float[] getPosition() {
        return position;
    }

    public void setPosition(float[] position) {
        this.position = position;
    }

    //endregion GetterSetter
}
