package epita.velibmapper.dbo;

/**
 * Created by aboul on 18/05/2017.
 */

public class Data {
    private int nhits;
    private Parameters parameters;
    private Record[] records;
    private Facet_Groups[] facet_groups;

    public Data()
    {}

    public Data(int nhits, Parameters parameters, Record[] records)
    {
        this.nhits = nhits;
        this.parameters = parameters;
        this.records = records;
    }


    //region GetterSetter

    public int getNhits() {
        return nhits;
    }

    public void setNhits(int nhits) {
        this.nhits = nhits;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    public Record[] getRecords() {
        return records;
    }

    public void setRecords(Record[] records) {
        this.records = records;
    }

    public Facet_Groups[] getFacet_groups() {
        return facet_groups;
    }

    public void setFacet_groups(Facet_Groups[] facet_groups) {
        this.facet_groups = facet_groups;
    }

    //endregion GetterSetter
}
