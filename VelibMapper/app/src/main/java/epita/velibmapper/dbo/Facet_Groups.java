package epita.velibmapper.dbo;

/**
 * Created by aboul on 18/05/2017.
 */

public class Facet_Groups {
    private String name;
    private Facet[] facets;

    public Facet_Groups(String name, Facet[] facets)
    {
        this.name = name;
        this.facets = facets;
    }

    //region GetterSetter

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Facet[] getFacets() {
        return facets;
    }

    public void setFacets(Facet[] facets) {
        this.facets = facets;
    }

    //endregion GetterSetter
}
