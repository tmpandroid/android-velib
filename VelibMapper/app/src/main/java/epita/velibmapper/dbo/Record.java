package epita.velibmapper.dbo;

/**
 * Created by aboul on 18/05/2017.
 */

public class Record {
    private String datasetid;
    private String recordid;
    private Fields fields;
    private Geometry geometry;

    public Record (String datasetid, String recordid, Fields fields, Geometry geometry)
    {
        this.datasetid = datasetid;
        this.recordid = recordid;
        this.fields = fields;
        this.geometry = geometry;
    }

    public Fields GetField()
    {
        return fields;
    }

    //region GetterSetter

    public String getDatasetid() {
        return datasetid;
    }

    public void setDatasetid(String datasetid) {
        this.datasetid = datasetid;
    }

    public String getRecordid() {
        return recordid;
    }

    public void setRecordid(String recordid) {
        this.recordid = recordid;
    }

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }


    //endregion GetterSetter
}
