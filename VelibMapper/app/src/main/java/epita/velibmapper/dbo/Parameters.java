package epita.velibmapper.dbo;

/**
 * Created by aboul on 18/05/2017.
 */

public class Parameters {
    private String[] dataset;
    private String timezone;
    private int rows;
    private String format;
    private String[] facet;

    public Parameters(String[] dataset, String timezone, int rows, String format, String[] facet)
    {
        this.dataset = dataset;
        this.timezone = timezone;
        this.rows = rows;
        this.format = format;
        this.facet = facet;
    }

    //region GetterSetter

    public String[] getDataset() {
        return dataset;
    }

    public void setDataset(String[] dataset) {
        this.dataset = dataset;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String[] getFacet() {
        return facet;
    }

    public void setFacet(String[] facet) {
        this.facet = facet;
    }

    //endregion GetterSetter
}
