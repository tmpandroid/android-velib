package epita.velibmapper.dbo;

/**
 * Created by aboul on 18/05/2017.
 */

public class Facet {
    private String name;
    private String path;
    private int count;
    private String state;

    public Facet(String name, String path, int count, String state)
    {
        this.name = name;
        this.path = path;
        this.count = count;
        this.state = state;
    }

    //region GetterSetter

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    //endregion GetterSetter
}
