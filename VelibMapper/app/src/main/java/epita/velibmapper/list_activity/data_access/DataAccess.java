package epita.velibmapper.list_activity.data_access;

import epita.velibmapper.dbo.Data;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by aboul on 18/05/2017.
 */

public interface DataAccess {

    public static final String endpoint = "https://opendata.paris.fr/";

    @GET("api/records/1.0/search/?dataset=stations-velib-disponibilites-en-temps-reel&rows=100&facet=contract_name")
    Call<Data> listData();
}
