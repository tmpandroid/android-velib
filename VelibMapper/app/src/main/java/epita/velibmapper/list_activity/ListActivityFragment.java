package epita.velibmapper.list_activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import epita.velibmapper.R;
import epita.velibmapper.dbo.Record;
import epita.velibmapper.list_activity.adapter.RecycleViewAdapter;
import epita.velibmapper.list_activity.data_access.WSGetter;
import epita.velibmapper.dbo.Data;
import epita.velibmapper.shared.GlobalDatas;
import epita.velibmapper.shared.SearchDatasResult;

import static android.R.attr.data;

/**
 * Created by aboul on 18/05/2017.
 */

public class ListActivityFragment extends Fragment {

    public ListActivityFragment() {
    }

    private RecyclerView mRecyclerView;
    private RecycleViewAdapter RecycleView_Adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Data values;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        values = new Data();
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        RecycleView_Adapter = new RecycleViewAdapter(mRecyclerView);
        mRecyclerView.setAdapter(RecycleView_Adapter);

        ConnectivityManager connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            values = WSGetter.GetList(RecycleView_Adapter);

        }
    }

    public void UpdateList(String value)
    {
        Record[] mDataset = GlobalDatas.getInstance().getRecords();
        ArrayList<Record> cvt = new ArrayList<>();
        for (Record record: mDataset) {
            if (record.getFields().getName().contains(value.toUpperCase()))
                cvt.add(record);
        }

        Record[] result = new Record[cvt.size()];
        int index = 0;
        for (Record record: cvt)
        {
            result[index] = record;
            ++index;
        }

        RecycleView_Adapter.setDataSetR(result);
        RecycleView_Adapter.notifyDataSetChanged();
        SearchDatasResult.SetData(result);
    }

    public void Recover()
    {
        RecycleView_Adapter.setDataSetR(GlobalDatas.getInstance().getRecords());
        RecycleView_Adapter.notifyDataSetChanged();
        SearchDatasResult.SetData(GlobalDatas.getInstance().getRecords());
    }
}
