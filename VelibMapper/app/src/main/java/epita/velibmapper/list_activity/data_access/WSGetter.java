package epita.velibmapper.list_activity.data_access;

import epita.velibmapper.list_activity.ListActivity;
import epita.velibmapper.list_activity.adapter.RecycleViewAdapter;
import epita.velibmapper.dbo.Data;
import epita.velibmapper.shared.GlobalDatas;
import epita.velibmapper.shared.SearchDatasResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aboul on 18/05/2017.
 */

public class WSGetter {

    private static Data data;

    public static Data GetList(final RecycleViewAdapter adapter)
    {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(DataAccess.endpoint).addConverterFactory(GsonConverterFactory.create()).build();
        DataAccess dataAccess = retrofit.create(DataAccess.class);

        Call<Data> datalist = dataAccess.listData();
        datalist.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                if(response.isSuccessful()) {
                    data = (Data)response.body();
                    adapter.setDataSet(data);
                    adapter.notifyDataSetChanged();
                    GlobalDatas.SetNr(true);
                    SearchDatasResult.SetData(data.getRecords());
                } else {
                    //Error
                }
            }
            @Override
            public void onFailure(Call<Data> call, Throwable t) {
            }
        });

        return data;
    }
}
