package epita.velibmapper.list_activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import epita.velibmapper.DetailActivity.DetailActivity;
import epita.velibmapper.R;
import epita.velibmapper.dbo.Data;
import epita.velibmapper.dbo.Record;
import epita.velibmapper.shared.GlobalDatas;
import epita.velibmapper.shared.Shared;
import epita.velibmapper.shared.StationNameCleaner;

/**
 * Created by aboul on 18/05/2017.
 */

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder>{

    private Record[] mDataset;
    private RecyclerView recyclerView;

    public RecycleViewAdapter(RecyclerView recycleview)
    {
        this.recyclerView = recycleview;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextView;
        private ImageView mStatus;
        private TextView mPlace;
        private TextView mAddress;

        public ViewHolder(View v) {
            super(v);
            mTextView = (TextView) v.findViewById(R.id.title);

            mStatus = (ImageView) v.findViewById(R.id.status);

            mAddress = (TextView) v.findViewById(R.id.address);
            mPlace = (TextView) v.findViewById(R.id.place);
        }
    }

    public void setDataSet(Data data) {
        mDataset = data.getRecords();
        GlobalDatas.SetData(data);
    }

    public void setDataSetR(Record[] records)
    {
        mDataset = records;
    }

    @Override
    public RecycleViewAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_content, parent, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = parent.getContext();
                Intent intent = new Intent(context, DetailActivity.class);
                int index = recyclerView.getChildAdapterPosition(v);
                intent.putExtra(Shared.INDEX_ARG, index);
                context.startActivity(intent);
            }
        });
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextView.setText(StationNameCleaner.Clean(mDataset[position].GetField().getName()));

        if (mDataset[position].GetField().getStatus().equals("CLOSED"))
            holder.mStatus.setImageResource(R.drawable.nop);
        else
            holder.mStatus.setImageResource(R.drawable.open);

        holder.mAddress.setText(mDataset[position].GetField().getAddress());

        int available_stand = mDataset[position].GetField().getAvailable_bike_stands();
        int total_stand = mDataset[position].GetField().getBike_stands();
        if (available_stand == 0) {
            holder.mPlace.setTextColor(Color.RED);
        }
        else
        {
            holder.mPlace.setTextColor(Color.parseColor("#373737"));
        }
        holder.mPlace.setText(available_stand + "/" + total_stand);

    }

    @Override
    public int getItemCount() {
        if (mDataset == null) return 0;
        return mDataset.length;
    }


}
